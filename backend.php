<?php
/* File:   backend.php
 * Author: Daniel Pavlick <dkp35@njit.edu>
 */

include "be_users.php";     // authenticate, register, and getters for user data
include "be_questions.php"; // getters and setters for question data
include "be_exams.php";     // getters and setters for exam data
include "be_scores.php";    // score/grade getters and setters

// shortcut function for pulling out SQL result when that result is
// only a single value (otherwise need to iterate manually)
function getResult($query) {
    $query->execute();
    $query->bind_result($result);
    $query->fetch();
    $query->close();
    return $result;    
}

// check for the existence of a valid authtoken in the database
// exit with error if no/stale token, continue operating otherwise
function checkAuthtoken($db, $request) {
    // do we have an authtoken to check?
    if (!property_exists($request, "authtoken")) {
        sendResponse(0, "Authtoken required.", null);
    }
    $authtoken = $request->authtoken;
    $query = $db->prepare("select timestamp from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $currTime = time();
    $storedTime = getResult($query);

    if (!$storedTime) { // no rows found, authtoken does not exist
        sendResponse(0, "Invalid authtoken", null);
    } elseif ($currTime - $storedTime > 3600) { // stale timestamp
        $query = $db->prepare("delete from authtokens where authtoken = ?");
        $query->bind_param("s", $authtoken);
        $query->execute();
        $query->close();
        sendResponse(0, "Expired authtoken", null);
    } else { // valid authtoken, refresh the timestamp
        $query = $db->prepare("update authtokens set timestamp = ? " .
                              "where authtoken = ?");
        $query->bind_param("is", $currTime, $authtoken);
        $query->execute();
        $query->close();
    }
}

// build and return the respone object for the middle
function sendResponse($statusCode, $errorMessage, $responseData) {
    $backendResponse = new stdClass;
    $backendResponse->statusCode = $statusCode;

    if ($statusCode == 0) {
        $backendResponse->errorMessage = $errorMessage;
    } elseif ($responseData != null) {
        foreach($responseData as $key => $value) {
            $backendResponse->$key = $value;
        }
    }
    
    exit(json_encode($backendResponse));
}

// decode incoming request
$request = json_decode(file_get_contents("php://input"));

// create and test database connection
$db = new mysqli('sql2.njit.edu', 'dkp35', 'pickleddatabase', 'dkp35');
if ($db->connect_error) {
    sendResponse(0, "Could not connect to database.", null);
}

// is an action specified?
if (!property_exists($request, "action")) {
    sendResponse(0, "No action specified.", null);
}

// check for current authentication
// (except for two actions that don't require auth to complete)
if (($request->action != "authenticate") && ($request->action != "register")) {
    checkAuthtoken($db, $request);
}

// check for request type and direct to appropriate function
switch ($request->action) {
    case "authenticate":
        authenticate($db, $request);        // be_users.php
        break;
    case "register":
        register($db, $request);            // be_users.php
        break;
    case "getUsername":
//        getUserame($db, $request);        // be_users.php
        break;
    case "getRole":
        getRole($db, $request);             // be_users.php
        break;
    case "getName":
        getName($db, $request);             // be_users.php
        break;
    case "deauthenticate":
        deauthenticate($db, $request);      // be_users.php
        break;
    case "getQuestionIDs":
        getQuestionIDs($db, $request);      // be_questions.php
        break;
    case "getQuestion":
        getQuestion($db, $request);         // be_questions.php
        break;
    case "createQuestion":
        createQuestion($db, $request);      // be_questions.php
        break;
    case "deleteQuestion":
        deleteQuestion($db, $request);      // be_questions.php
        break;
    case "getUnusedQuestions":
        getUnusedQuestions($db, $request);  // be_questions.php
        break;
    case "getExamIDs":
        getExamIDs($db, $request);          // be_exams.php
        break;
    case "getOpenExamIDs":
        getOpenExamIDs($db, $request);      // be_exams.php
        break;
    case "getExamIDsByTeacher":
        getExamIDsByTeacher($db, $request); // be_exams.php
        break;
    case "getAvailExamIDs":
        getAvailExamIDs($db, $request);     // be_exams.php
        break;
    case "getExamName":
        getExamName($db, $request);         // be_exams.php
        break;
    case "getExamQuestions":
        getExamQuestions($db, $request);    // be_exams.php
        break;
    case "getExamStatus":
        getExamStatus($db, $request);       // be_exams.php
        break;
    case "openExam":
        openExam($db, $request);            // be_exams.php
        break;
    case "closeExam":
        closeExam($db, $request);           // be_exams.php
        break;
    case "createExam":
        createExam($db, $request);          // be_exams.php
        break;
    case "addQuestion":
        addQuestion($db, $request);         // be_exams.php
        break;
    case "removeQuestion":
        removeQuestion($db, $request);      // be_exams.php
        break;
    case "getExamStats":
        getExamStats($db, $request);        // be_exams.php
        break;
    case "createAttempt":
        createAttempt($db, $request);       // be_scores.php
        break;
    case "setScore":
        setScore($db, $request);            // be_scores.php
        break;
    case "getExamsByUser":
        getExamsByUser($db, $request);      // be_scores.php
        break;
    case "getUsersByExam":
        getUsersByExam($db, $request);      // be_scores.php
        break;
    case "getScore":
        getScore($db, $request);            // be_scores.php
        break;
    case "getScoresByUser":
        getScoresByUser($db, $request);     // be_scores.php
        break;
    case "getFeedback":
        getFeedback($db, $request);         // be_scores.php
        break;
    default:
        sendResponse(0, "Unsupported backend action.", null);
}

$db->close();
?>
