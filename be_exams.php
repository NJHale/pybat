<?php
/* File:   be_exams.php
 * Author: Daniel Pavlick <dkp35@njit.edu>
 */
 
function getExamIDs($db, $request) {
    $query = $db->prepare("select eid from exams");
    $query->execute();
    $query->bind_result($eid);
    $eids = null;
    while ($query->fetch()) {
        $eids[] = $eid;
    }
    
    $responseData = new stdClass;
    $responseData->examIDs = $eids;
    sendResponse(1, null, $responseData);
}

function getOpenExamIDs($db, $request) {
    $query = $db->prepare("select eid from exams where status = \"open\"");
    $query->execute();
    $query->bind_result($eid);
    $eids = null;
    while ($query->fetch()) {
        $eids[] = $eid;
    }
    
    $responseData = new stdClass;
    $responseData->examIDs = $eids;
    sendResponse(1, null, $responseData);
}

function getExamIDsByTeacher($db, $request) {
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);

    $query = $db->prepare("select eid from exams where owner = ?");
    $query->bind_param("s", $owner);
    $query->execute();
    $query->bind_result($eid);
    $eids = [];
    while($query->fetch()) {
        $eids[] = $eid;
    }
    
    $responseData = new stdClass;
    $responseData->eids = $eids;
    sendResponse(1, null, $responseData);
}
    
function getAvailExamIDs($db, $request) {
    $authtoken = $request->authtoken;
    // get open exams
    $query = $db->prepare("select eid from exams where status = \"open\"");
    $query->execute();
    $query->bind_result($openID);
    $openIDs = [];
    while ($query->fetch()) {
        $openIDs[] = $openID;
    }
    $query->close();
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $uid = getResult($query);
    
    // get taken exams
    $query = $db->prepare("select eid from attempts where uid = ?");
    $query->bind_param("s", $uid);
    $query->execute();
    $query->bind_result($takenID);
    $takenIDs = [];
    while ($query->fetch()) {
        $takenIDs[] = $takenID;
    }
    
    $availIDs = array_diff($openIDs, $takenIDs);
    
    $responseData = new stdClass;
    $responseData->examIDs = $availIDs;
    sendResponse(1, null, $responseData);
}

function getExamName($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("select name from exams where eid = ?");
    $query->bind_param("i", $eid);
    $name = getResult($query);
    if (!$name) {
        sendResponse(0, "No exam with that ID exists.", null);
    }
    
    $responseData = new stdClass;
    $responseData->examName = $name;
    $responseData->examID = $eid;
    sendResponse(1, null, $responseData);
}

function getExamQuestions($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("select questions from exams where eid = ?");
    $query->bind_param("i", $eid);
    $questions = getResult($query);
    if (!$questions) {
        sendResponse(0, "No exam with that ID exists.", null);
    }
    
    $responseData = new stdClass;
    $responseData->questions = json_decode($questions);
    sendResponse(1, null, $responseData);
}

function getExamStatus($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("select status from exams where eid = ?");
    $query->bind_param("i", $eid);
    $status = getResult($query);
    if (!$status) {
        sendResponse(0, "No exam with that ID exists.", null);
    }
    
    $responseData = new stdClass;
    $responseData->status = $status;
    sendResponse(1, null, $responseData);    
}

function openExam($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("update exams set status = \"open\" where eid = ?");
    $query->bind_param("i", $eid);
    $query->execute();
    $query->close();
    sendResponse(1, null, null);
}

function closeExam($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("update exams set status = \"closed\" where eid = ?");
    $query->bind_param("i", $eid);
    $query->execute();
    $query->close();
    
    // release scores
    $query = $db->prepare("update attempts set status = \"released\" where eid = ?");
    $query->bind_param("i", $eid);
    $query->execute();
    $query->close();

    sendResponse(1, null, null);    
}

function createExam($db, $request) {
    if (!property_exists($request, "examName")) {
        sendResponse(0, "No exam name provided.", null);
    }
    $name = $request->examName;

    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);

    $query = $db->prepare("insert into exams (owner, name, questions, status) values (?, ?, \"[]\", \"hidden\")");
    $query->bind_param("ss", $owner, $name);
    $query->execute();
    
    $eid = $db->insert_id;
    $responseData = new stdClass;
    $responseData->examID = $eid;
    sendResponse(1, null, $responseData);
}

function addQuestion($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "questionID")) {
        sendResponse(0, "No question ID provided.", null);
    } else if (!property_exists($request, "checkLoops")) {
        sendResponse(0, "No checkLoops flag provided.", null);
    } else if (!property_exists($request, "checkRecursion")) {
        sendResponse(0, "No checkRecursion flag provided.", null);
    } else if (!property_exists($request, "weight")) {
        sendResponse(0, "No weight provided.");
    }
    
    $eid = $request->examID;

    $question = new stdClass;
    $question->questionID = $request->questionID;
    $question->checkLoops = $request->checkLoops;
    $question->checkRecursion = $request->checkRecursion;
    $question->weight = $request->weight;
    $query = $db->prepare("select questions from exams where eid = ?");
    $query->bind_param("i", $eid);
    $questions = json_decode(getResult($query));
    $questions[] = $question;
    $questions = json_encode($questions);
    
    $query = $db->prepare("update exams set questions = ? where eid = ?");
    $query->bind_param("si", $questions, $eid);
    $query->execute();
    sendResponse(1, null, null);
}
    
function removeQuestion($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID given.", null);
    } elseif (!property_exists($request, "questionID")) {
        sendResponse(0, "No question ID given.");
    }
    $eid = $request->examID;
    $qid = $request->questionID;
    
    $query = $db->prepare("select questions from exams where eid = ?");
    $query->bind_param("i", $eid);
    $questions = json_decode(getResult($query));
    $newQuestions = [];
    foreach ($questions as $value) {
        if ($value->questionID != $qid) {
            $newQuestions[] = $value;
        }
    }
    $newQuestions = json_encode($newQuestions);
    
    $query = $db->prepare("update exams set questions = ? where eid = ?");
    $query->bind_param("si", $newQuestions, $eid);
    $query->execute();
    sendResponse(1, null, null);
}

function getExamStats($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $examID = $request->examID;
    
    // get scores
    $query = $db->prepare("select score from attempts where eid = ?");
    $query->bind_param("i", $examID);
    $query->execute();
    $scores = [];
    $query->bind_result($score);
    while ($query->fetch()) {
        $scores[] = $score;
    }
    $query->close();

    $n = count($scores);
    if ($n == 0) {
        $min = 0;
        $max = 0;
        $mean = 0;
        $sd = 0;
    } else {
        $min = 100;
        $max = 0;
        $total = 0;
        foreach ($scores as $value) {
            $total += $value;
            if ($value > $max) {
                $max = $value;
            }
            if ($value < $min) {
                $min = $value;
            }
        }
    
        $mean = $total / $n;
    
        $totalDeviation = 0;
        foreach ($scores as $value) {
            $totalDeviation += pow($mean - $value, 2);
        }
    
        $variance = $totalDeviation / $n;
        $sd = sqrt($variance);
    }
    
    $responseData = new stdClass;
    $responseData->examID = $examID;
    $responseData->n = $n;
    $responseData->min = $min;
    $responseData->max = $max;
    $responseData->mean = $mean;
    $responseData->sd = $sd;
    sendResponse(1, null, $responseData);
}

?>
