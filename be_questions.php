<?php
/* File:   be_questions.php
 * Author: Daniel Pavlick <dkp35@njit.edu>
 */
 
function getQuestionIDs($db, $request) {
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);
    
    // get questions belonging to owner
    $query = $db->prepare("select qid from questions where owner = ?");
    $query->bind_param("s", $owner);
    $query->execute();
    $query->bind_result($qid);
    $qids = null;
    while ($query->fetch()) {
        $qids[] = $qid;
    }
    $query->close();
    
    $responseData = new stdClass;
    $responseData->questionIDs = $qids;
    sendResponse(1, null, $responseData);
}

function getQuestion($db, $request) {
    if (!property_exists($request, "questionID")) {
        sendResponse(0, "No question ID provided.", null);
    }
    $qid = $request->questionID;
    $query = $db->prepare("select text, funcname, parameters, testcases from questions where qid = ?");
    $query->bind_param("i", $qid);
    $query->execute();
    $query->bind_result($text, $funcname, $parameters, $testcases);
    $query->fetch();
    $query->close();
    
    if (!$text) { // there was no such question with that id
        sendResponse(0, "No question with that ID exists.", null);
    } else {
        $responseData = new stdClass;
        $responseData->questionID = $qid;
        $responseData->text = $text;
        $responseData->funcname = $funcname;
        $responseData->parameters = json_decode($parameters);
        $responseData->testcases = json_decode($testcases);
        sendResponse(1, null, $responseData);
    }
}

function createQuestion($db, $request) {
    // error checking
    if (!property_exists($request, "text")) {
        sendResponse(0, "No question text provided.", null);
    } else if (!property_exists($request, "funcname")) {
        sendResponse(0, "No function name provided.", null);
    } else if (!property_exists($request, "parameters")) {
        sendResponse(0, "No parameter list provided. If there are no parameters for this question, please pass an empty array (i.e. \"[]\").", null);
    } else if (!property_exists($request, "testcases")) {
        sendResponse(0, "No test cases provided.", null);
    }

    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);

    $text = $request->text;
    $funcname = $request->funcname;
    $parameters = json_encode($request->parameters);
    $testcases = json_encode($request->testcases);
    $query = $db->prepare("insert into questions (text, funcname, parameters, testcases, owner) values (?, ?, ?, ?, ?)");
    $query->bind_param("sssss", $text, $funcname, $parameters, $testcases, $owner);
    $query->execute();
    
    if($query->error) {
        $questionID = 99;
        sendResponse(0, $query->error, null);
    } else {    
        $questionID = $db->insert_id;
        $responseData = new stdClass;
        $responseData->questionID = $questionID;
        sendResponse(1, null, $responseData);
    }
}

function deleteQuestion($db, $request) {
    // error checking
    if (!property_exists($request, "questionID")) {
        sendResponse(0, "No question ID provided.", null);
    }
    $questionID = $request->questionID;
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);

    // get all exams by user
    $query = $db->prepare("select questions from exams where owner = ?");
    $query->bind_param("s", $owner);
    $query->execute();
    $query->bind_result($questions);
    while ($query->fetch()) {
        foreach (json_decode($questions) as $question) {
            if ($question == $questionID) {
                sendResponse(0, "That question is used in an exam and cannot be deleted.", null);
            }
        }
    }
    $query->close();
    
    $query = $db->prepare("delete from questions where qid = ?");
    $query->bind_param("i", $questionID);
    $query->execute();
    
    if ($query->error) {
        sendResponse(0, "Error deleting the question.", null);
    } else {
        sendResponse(1, null, null);
    }
}

function getUnusedQuestions($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID supplied.", null);
    }
    $eid = $request->examID;
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $request->authtoken);
    $owner = getResult($query);
    
    $query = $db->prepare("select questions from exams where eid = ?");
    $query->bind_param("i", $eid);
    $examQuestions = json_decode(getResult($query));
    $examQuestionIDs = [];
    foreach($examQuestions as $question) {
        $examQuestionIDs[] = $question->questionID;
    }
    
    $query = $db->prepare("select qid from questions where owner = ?");
    $query->bind_param("s", $owner);
    $query->execute();
    $query->bind_result($questionID);
    $questionIDs = [];
    while($query->fetch()) {
        $questionIDs[] = $questionID;
    }
    
    $unused = array_diff($questionIDs, $examQuestionIDs);
    
    $responseData = new stdClass;
    $responseData->questionIDs = $unused;
    sendResponse(1, null, $responseData);
}

?>
