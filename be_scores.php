<?php
/* File:   be_scores.php
 * Author: Daniel Pavlick <dkp35@njit.edu>
 */

function createAttempt($db, $request) {
    $authtoken = $request->authtoken;
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "answers")) {
        sendResponse(0, "No answers provided (send an empty array - \"[]\" - if you have no answers to insert.", null);
    } else if (!property_exists($request, "feedback")) {
        sendResponse(0, "No feedback provided (send an empty array - \"[]\" - if you have no feedback to insert.", null);
    } else if (!property_exists($request, "score")) {
        sendResponse(0, "No score was provided.", null);
    }
    $eid = $request->examID;
    $answers = json_encode($request->answers);
    $feedback = json_encode($request->feedback);
    $score = $request->score;
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);
    
    $query = $db->prepare("insert into attempts (uid, eid, score, answers, feedback, status) values (?, ?, ?, ?, ?, \"locked\")");
    $query->bind_param("siiss", $uid, $eid, $score, $answers, $feedback);
    $query->execute();
    sendResponse(1, null, null);
}

function setAnswers($db, $request) {
    $authtoken = $request->authtoken;
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "answers")) {
        sendResponse(0, "No answers provided (send an empty array - \"[]\" - if you have no answers to insert.", null);
    }
    $eid = $request->examID;
    $answers = $request->answers;
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);
    
    $query = $db->prepare("update attempts set answers = ? where uid = ? and eid = ?");
    $query->bind_param("ssi", $answers, $uid, $eid);
    $query->execute();
    sendResponse(1, null, null);
}

function appendAnswer($db, $request) {
    $authtoken = $request->authtoken;
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "answers")) {
        sendResponse(0, "No answers provided (send an empty array - \"[]\" - if you have no answers to insert.", null);
    }
    $eid = $request->examID;
    $newAnswers = json_decode($request->answers);
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);
    
    // get current answers and append new one to list
    $query = $db->prepare("select answers from attempts where uid = ? and eid = ?");
    $query->bind_param("si", $uid, $eid);
    $curAnswers = json_decode(getResult($query));
    $curAnswers[] = $newAnswers;
    $curAnswers = json_encode($curAnswers);
    
    $query = $db->prepare("update attempts set answers = ? where uid = ? and eid = ?");
    $query->bind_param("ssi", $curAnswers, $uid, $eid);
    $query->execute();
    sendResponse(1, null, null);
}

function setScore($db, $request) {
    $authtoken = $request->authtoken;
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "score")) {
        sendResponse(0, "No score provided.", null);
    }
    $eid = $request->examID;
    $score = $request->score;
    
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);
    
    $query = $db->prepare("update attempts set score = ? where uid = ? and eid = ?");
    $query->bind_param("isi", $score, $uid, $eid);
    $query->execute();
    sendResponse(1, null, null);
}

function getExamsByUser($db, $request) {
    if (!property_exists($request, "username")) {
        sendResponse(0, "No username provided.", null);
    }
    $uid = $request->username;
    $query = $db->prepare("select eid from attempts where uid = ?");
    $query->bind_param("s", $uid);
    $query->execute();
    $query->bind_result($eid);
    $eids = null;    
    while ($query->fetch()) {
        $eids[] = $eid;
    }

    $responseData = new stdClass;
    $responseData->examIDs = $eids;
    sendResponse(1, null, $responseData);
}

function getUsersByExam($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    $query = $db->prepare("select uid from attempts where eid = ?");
    $query->bind_param("i", $eid);
    $query->execute();
    $query->bind_result($uid);
    $uids = null;    
    while ($query->fetch()) {
        $uids[] = $uid;
    }

    $responseData = new stdClass;
    $responseData->usernames = $uids;
    sendResponse(1, null, $responseData);
}

function getScore($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    } else if (!property_exists($request, "username")) {
        sendResponse(0, "No username provided.", null);
    }
    $eid = $request->examID;
    $uid = $request->username;
    $query = $db->prepare("select score, feedback from attempts where eid = ? and uid = ?");
    $query->bind_param("is", $eid, $uid);

    $score = null;
    $feedback = "[]";
    $query->bind_result($score, $feedback);
    $query->fetch();
    if ($score === null) {
        sendResponse(0, "That user has no score for that exam.", null);
    }
    
    $responseData = new stdClass;
    $responseData->score = $score;
    $responseData->feedback = $feedback;
    sendResponse(1, null, $responseData);
}

function getScoresByUser($db, $request) {
    $authtoken = $request->authtoken;
    // get uid from authtoken
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);

    $query = $db->prepare("select eid, score from attempts where uid = ? and status = \"released\"");
    $query->bind_param("s", $uid);
    $query->execute();
    $query->bind_result($examID, $score);
    $examIDs = [];
    $scores = [];
    while ($query->fetch()) {
        $examIDs[] = $examID;
        $scores[] = $score;
    }
    $query->close();
    
    $examNames = [];
    foreach($examIDs as $value) {
        $query = $db->prepare("select name from exams where eid = ?");
        $query->bind_param("i", $value);
        $examNames[] = getResult($query);
    }
    
    $responseData = new stdClass;
    $responseData->examIDs = $examIDs;
    $responseData->examNames = $examNames;
    $responseData->scores = $scores;
    sendResponse(1, null, $responseData);
}

function getFeedback($db, $request) {
    if (!property_exists($request, "examID")) {
        sendResponse(0, "No exam ID provided.", null);
    }
    $eid = $request->examID;
    
    // get uid from authtoken
    $authtoken = $request->authtoken;
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $uid = getResult($query);

    $query = $db->prepare("select feedback from attempts where uid = ? and eid = ?");
    $query->bind_param("si", $uid, $eid);
    $query->execute();
    $result = getResult($query);
    
    $responseData = new stdClass;
    $responseData->feedback = $result;
    sendResponse(1, null, $responseData);
}

?>
