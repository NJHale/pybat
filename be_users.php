<?php
/* File:   be_users.php
 * Author: Daniel Pavlick <dkp35@njit.edu>
 */

function authenticate($db, $request) {
    if (!property_exists($request, "username")) {
        sendResponse(0, "No username provided.", null);
    } else if (!property_exists($request, "password")) {
        sendResponse(0, "No password provided.", null);
    }
    $user = $request->username;
    $pass = $request->password;

    // pull out results that match the given user name
    $query = $db->prepare("select password from users where uid = ?");
    $query->bind_param("s", $user);
    $storedPass = getResult($query);

    // does the user exist?
    if (!$storedPass) {
        sendResponse(0, "That user does not exist.", null);
    }

    // does the password match?
    $hashedPass = crypt($pass, $storedPass);
    if (!($storedPass == $hashedPass)) {
        sendResponse(0, "Incorrect password.", null);
    } else { // we have a match
        // create authtoken
        $time = time();
        $authtoken = crypt($user . $time);
        $dollarpos = strrpos($authtoken, "$");
        $authtoken = substr($authtoken, $dollarpos + 1);
        
        // add authtoken to database
        // check if user already has an authtoken
        $query = $db->prepare("select authtoken from authtokens where uid = ?");
        $query->bind_param("s", $user);
        $storedAuthtoken = getResult($query);
        if (!$storedAuthtoken) { // insert
            $query = $db->prepare("insert into authtokens " .
                     "(uid, timestamp, authtoken) " .
                     "values (?, ?, ?)");
            $query->bind_param("sis", $user, $time, $authtoken);
        } else { // update
            $query = $db->prepare("update authtokens " .
                     "set timestamp = ?, authtoken = ? " .
                     "where uid = ?");
            $query->bind_param("iss", $time, $authtoken, $user);
        }
        $query->execute();
        $query->close();

        $responseData = new stdClass;
        $responseData->authtoken = $authtoken;
        sendResponse(1, null, $responseData);
    }
}

function register($db, $request) {
    // probably unnecessary as the front checks for these on client-side...
    if (!property_exists($request, "username")) {
        sendResponse(0, "No username provided.", null);
    } elseif (!property_exists($request, "password")) {
        sendResponse(0, "No password provided.", null);
    } elseif (!property_exists($request, "accountType")) {
        sendResponse(0, "No account type specified.", null);
    } elseif ($request->accountType != "student" && 
              $request->accountType != "teacher") {
        sendResponse(0, "Invalid account type.", null);
    } elseif (!property_exists($request, "firstname")) {
        sendResponse(0, "First name required.", null);
    } elseif (!property_exists($request, "lastname")) {
        sendResponse(0, "Last name required.", null);
    }
    
    $user = $request->username;
    $pass = $request->password;
    $role = $request->accountType;
    $fname = $request->firstname;
    $lname = $request->lastname;
        
    // check if user name is taken
    $query = $db->prepare("select uid from users where uid = ?");
    $query->bind_param("s", $user);
    if (getResult($query)) { // we have a match
        sendResponse(0, "That username is already in use.", null); 
    }
    
    // hash password
    $pass = crypt($pass);
    
    // store all three and return success
    $query = $db->prepare("insert into users (uid, password, role, fname, lname) values(?, ?, ?, ?, ?)");
    $query->bind_param("sssss", $user, $pass, $role, $fname, $lname);
    $query->execute();
    if ($query->errno == 0) { // no error has occurred
        sendResponse(1, null, null);
    } else {
        sendResponse(0, "There was an error adding the user to the database.", null);
    }
}

function getUsername($db, $request) {
    $authtoken = $request->authtoken; // checkAuthtoken assures we have a token
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $result = getResult($query);
    
    $responseData = new stdClass;
    $responseData->username = $result;
    sendResponse(1, null, $responseData);
}

function getRole($db, $request) {
    // get uid from authtoken
    $authtoken = $request->authtoken; // checkAuthtoken assures we have a token
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $result = getResult($query);
    
    // get role from uid
    $query = $db->prepare("select role from users where uid = ?");
    $query->bind_param("s", $result);
    $result = getResult($query);
    
    $responseData = new stdClass;
    $responseData->role = $result;
    sendResponse(1, null, $responseData);
}

function getName($db, $request) {
    // get uid from authtoken
    $authtoken = $request->authtoken; // checkAuthtoken assures we have a token
    $query = $db->prepare("select uid from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $result = getResult($query);

    // get names from uid
    $query = $db->prepare("select fname, lname from users where uid = ?");
    $query->bind_param("s", $result);
    $query->execute();
    $query->bind_result($fname, $lname);
    $query->fetch();
    $query->close();
        
    $responseData = new stdClass;
    $responseData->firstname = $fname;
    $responseData->lastname = $lname;
    
    sendResponse(1, null, $responseData);
}
    
function deauthenticate($db, $request) {
    $authtoken = $request->authtoken;
    $query = $db->prepare("delete from authtokens where authtoken = ?");
    $query->bind_param("s", $authtoken);
    $query->execute;
    
    sendResponse(1, null, null);
}

?>
