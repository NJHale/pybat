<?php
	//transparent proxy for json HTTP POST requests
	
	//forward http request and header to middle end
	//$midend_url = 'https://web.njit.edu/~afn5/middle.php';
	//$midend_url = 'https://web.njit.edu/~njh4/CS490/releaseCandidate/middle.php';
	//$midend_url = 'https://web.njit.edu/~dkp35/middle.php';
	//$midend_url = 'https://web.njit.edu/~dkp35/middle.php?';
	$midend_url = 'http://osl84.njit.edu/~dkp35/middle.php';
	//get body of the request as raw data
	$request = file_get_contents("php://input");

	//open connection
	$ch = curl_init($midend_url);

	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
    //Set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

	//execute post and retrieve result
	$result = curl_exec($ch);
	//get info on the http request
	$info = curl_getinfo($ch);
	//set forwarded response code
	http_response_code($info['http_code']);
	//close connection
	curl_close($ch);
	exit($result);
	
?>
