//Nick Hale njh4@njit.edu

$( document ).ready(function(){
	
	$("#register-link").on('click', function() {
		$("#register-container").show();
	});
	
	$("#register-form").submit(function(event){
		$("#success-box").hide()
		$("#warning-box").hide()
		//prevent default submission
		event.preventDefault()
		//gather form data
		var formData = serializeObject('#register-form');
		//$("#register-form").serializeArray().map(function(x){formData[x.name] = x.value;})
		//set action
		var data = {
			'action': 'register'
		};
		$.extend(data, formData)
		//send AJAX request
		try {
			sendAjaxRequest(data, registerCallback);
		} catch (err) {
			$("#warning-text").text("There were problems registering.  Ben! I can be a Jedi. Ben, tell him I’m ready!")
			$("#warning-box").show()
			$("#warning-box").focus()
			console.log(err);
			alert(err)
		}
		return false;
	});
	
	$("#login-form").submit(function(event){
		//hide alert boxes
		$("#success-box").hide()
		$("#warning-box").hide()
		//prevent default submission
		event.preventDefault()
		//gather form data
		var formData = {};
		//$("#login-form").serializeArray().map(function(x){formData[x.name] = x.value;}); 
		formData = serializeObject('#login-form');
		var data = {
			'action': 'authenticate',
		};
		//merge form data and action
		$.extend(data, formData)
		//send AJAX request
		try {
			sendAjaxRequest(data, loginCallback);
		} catch (err) {
			$("#warning-text").text("There were problems authenticating. Luck? In my experience there's no such thing as luck.")
			$("#warning-box").show()
			$("#warning-box").focus()
			console.log(err);
			alert(err)
		}
		return false;
	});
	
	//login callback function
	function loginCallback(data, status) {
		//state machine for handling AJAX return status cases
		switch (status) {
			case 200://successful status
				//display temporary success notification
				$("#success-text").text("Authentication Successful. Never tell me the odds!")
				$("#success-box").show()
				$("#success-box").focus()
				//store authToken
				sessionStorage.authtoken = data.authtoken
				//get user role
				var data= {
					'action': 'getRole',
					'authtoken': sessionStorage.authtoken
				};
				sendAjaxRequest(data, getRollCallback);
				break
			case 400://general failure status
				$("#warning-text").text("There were problems authenticating. Luck? In my experience there's no such thing as luck.")
				$("#warning-box").show()
				$("#warning-box").focus()
				break
			default:
				$("#warning-text").text("There are some issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	//getRollCallback
	function getRollCallback(data, status) {
		//state machine for handling AJAX return status cases
		switch (status) {
			case 200://successful status
				//store user role
				sessionStorage.role = data.role
				//handle redirects based on role
				if (sessionStorage.role == 'teacher')
					redirect('teacher.html')
				if (sessionStorage.role == 'student')
					redirect('student.html')
				break
			case 400://general failure status
				$("#warning-text").text("There were problems authenticating. Luck? In my experience there's no such thing as luck.")
				$("#warning-box").show()
				$("#warning-box").focus()
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	//register callback function
	function registerCallback(data, status) {
		//state machine for handling AJAX return status cases
		switch(status) {
			case 200://successful status
				$("#success-text").text("Registration Successful! May the Force be with you!")
				$("#register-container").hide()
				$("#success-box").show()
				$("#success-box").focus()
				break;
			case 400://general failure status
				$("#warning-text").text("There were problems registering. Ben! I can be a Jedi. Ben, tell him I’m ready!")
				$("#warning-box").show()
				$("#warning-box").focus()
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
});

