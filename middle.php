<?php

include "middle_utils.php";
include "middle_users.php";
include "middle_exams.php";
include "middle_python_runner.php";

$request = parse_frontend_request(file_get_contents("php://input")); // Get the body of the request

switch ($request->action) {
    case "authenticate":
        sendResponseAndExit(authenticate($request->username, $request->password));
        break;
    case "register":
        sendResponseAndExit(register($request->username, $request->password, $request->accountType, $request->firstname, $request->lastname));
        break;
    case "getUsername":
        sendResponseAndExit(getUsername($request->authtoken));
        break;
    case "getRole":
        sendResponseAndExit(getRole($request->authtoken));
        break;
    case "getName":
        sendResponseAndExit(getName($request->authtoken));
        break;
    case "deauthenticate":
        sendResponseAndExit(deauthenticate($request->authtoken));
        break;
    case "getQuestionIDs":
        sendResponseAndExit(getQuestionIDs($request->authtoken));
        break;
    case "getQuestion":
        sendResponseAndExit(getQuestion($request->authtoken, $request->questionID));
        break;
    case "createQuestion":
        sendResponseAndExit(createQuestion($request->authtoken, $request->text, $request->funcname, $request->parameters, $request->testcases));
        break;
    case "deleteQuestion":
        sendResponseAndExit(deleteQuestion($request->authtoken, $request->questionID));
        break;
    case "getUnusedQuestions":
        sendResponseAndExit(getUnusedQuestions($request->authtoken, $request->examID));
        break;
    case "getExamIDs":
        sendResponseAndExit(getExamIDs($request->authtoken));
        break;
    case "getOpenExamIDs":
        sendResponseAndExit(getOpenExamIDs($request->authtoken));
        break;
    case "getExamIDsByTeacher":
        sendResponseAndExit(getExamIDsByTeacher($request->authtoken));
        break;
    case "getAvailExamIDs":
        sendResponseAndExit(getAvailExamIDs($request->authtoken));
        break;
    case "getExamName":
        sendResponseAndExit(getExamName($request->authtoken, $request->examID));
        break;
    case "getExamQuestions":
        sendResponseAndExit(getExamQuestions($request->authtoken, $request->examID));
        break;
    case "getExamStatus":
        sendResponseAndExit(getExamStatus($request->authtoken, $request->examID));
        break;
    case "openExam":
        sendResponseAndExit(openExam($request->authtoken, $request->examID));
        break;
    case "closeExam":
        sendResponseAndExit(closeExam($request->authtoken, $request->examID));
        break;
    case "createExam":
        sendResponseAndExit(createExam($request->authtoken, $request->examName));
        break;
    case "addQuestion":
        sendResponseAndExit(addQuestion($request->authtoken, $request->examID, $request->questionID, $request->checkLoops, $request->checkRecursion, $request->weight));
        break;
    case "removeQuestion":
        sendResponseAndExit(removeQuestion($request->authtoken, $request->examID, $request->questionID));
        break;
    case "getExamStats":
        sendResponseAndExit(getExamStats($request->authtoken, $request->examID));
        break;
    case "setScore":
        sendResponseAndExit(setScore($request->authtoken, $request->examID, $request->score));
        break;
    case "getExamsByUser":
        sendResponseAndExit(getExamsByUser($request->username));
        break;
    case "getUsersByExam":
        sendResponseAndExit(getUsersByExam($request->examID));
        break;
    case "getScore":
        sendResponseAndExit(getScore($request->username, $request->examID));
        break;
    case "getScoresByUser":
        sendResponseAndExit(getScoresByUser($request->authtoken));
        break;
    case "getFeedback":
        sendResponseAndExit(getFeedback($request->authtoken, $request->examID));
        break;
    case "gradeAttempt":
        sendResponseAndExit(gradeAttempt($request->authtoken, $request->examID, $request->submittedAnswers));
        break;
    default:
        sendResponseAndExit(new Response(400, "Error: Invalid action"));
}

?>
