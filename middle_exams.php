<?php

function getQuestionIDs($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getQuestionIDs", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getQuestion($authtoken, $questionID) {
    $data = array('authtoken'=>$authtoken, 'questionID'=>$questionID);
    $result = parse_backend_response(sendBackendRequest("getQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function createQuestion($authtoken, $text, $funcname, $parameters, $testcases) {
    $data = array('authtoken'=>$authtoken, 'text'=>$text, 'funcname'=>$funcname, 'parameters'=>$parameters, 'testcases'=>$testcases);
    $result = parse_backend_response(sendBackendRequest("createQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function deleteQuestion($authtoken, $questionID) {
    $data = array('authtoken'=>$authtoken, 'questionID'=>$questionID);
    $result = parse_backend_response(sendBackendRequest("deleteQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getUnusedQuestions($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getUnusedQuestions", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamIDs($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getExamIDs", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getOpenExamIDs($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getOpenExamIDs", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamIDsByTeacher($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getExamIDsByTeacher", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getAvailExamIDs($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getAvailExamIDs", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamName($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getExamName", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamQuestions($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getExamQuestions", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamStatus($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getExamStatus", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function openExam($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("openExam", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function closeExam($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("closeExam", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function createExam($authtoken, $examName) {
    $data = array('authtoken'=>$authtoken, 'examName'=>$examName);
    $result = parse_backend_response(sendBackendRequest("createExam", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function addQuestion($authtoken, $examID, $questionID, $checkLoops, $checkRecursion, $weight) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID, 'questionID'=>$questionID, 'checkLoops'=>$checkLoops, 'checkRecursion'=>$checkRecursion, 'weight'=>$weight);
    $result = parse_backend_response(sendBackendRequest("addQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function removeQuestion($authtoken, $examID, $questionID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID, 'questionID'=>$questionID);
    $result = parse_backend_response(sendBackendRequest("removeQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamStats($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getExamStats", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function setScore($authtoken, $examID, $score) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID, 'score'=>$score);
    $result = parse_backend_response(sendBackendRequest("addQuestion", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getExamsByUser($username) {
    $data = array('username'=>$username);
    $result = parse_backend_response(sendBackendRequest("getExamsByUser", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getUsersByExam($examID) {
    $data = array('examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getUsersByExam", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getScore($username, $examID) {
    $data = array('username'=>$username, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getScore", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getScoresByUser($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getScoresByUser", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getFeedback($authtoken, $examID) {
    $data = array('authtoken'=>$authtoken, 'examID'=>$examID);
    $result = parse_backend_response(sendBackendRequest("getFeedback", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function submitAnswer($authtoken, $questionID, $submittedAnswer) {
    $data = array('authtoken'=>$authtoken, 'questionID'=>$questionID);
    $question = parse_backend_response(sendBackendRequest("getQuestion", $data));

    if ($question->statusCode === 0) {
        return new Response(400, "Error: " . $question->errorMessage);
    } else {
        $expected_output = $question->output;
        $py_answer = new PythonRunner($submittedAnswer);
        if ($py_answer->isSafeToRun() === FALSE) {
            return new Response(400, array('correct'=>false, 'output'=>"The code was not executed because it was deemed malicious"));
        } else {
            $output = $py_answer->execute();
            $correct = ($output === $expected_output);
            return new Response(200, array('correct'=>$correct, 'output'=>$output));
        }
    }
}

?>
