<?php

class PythonRunner {
    private $python_source;

    function __construct($source) {
        $this->python_source = $source;
    }

    /**
     * Does some basic checks on the python code to detect malicious code
     * Returns true if the code is deemed safe,
     */
    function isSafeToRun() {
        return (strpos($this->python_source, 'import') === FALSE // don't allow imports 
                && strpos($this->python_source, 'system') === FALSE // don't allow system calls
                && strpos($this->python_source, 'os') === FALSE // don't allow os calls
                && strpos($this->python_source, 'rm -r') === FALSE // c'mon
                && strpos($this->python_source, 'open(') === FALSE); // don't allow things to be opened
    }

    /**
     * Executes the python code and returns the result
     */
    function execute() {
        $filename = uniqid() . ".cs490.py"; // get a random file name

        $file = fopen($filename, "w");
        fwrite($file, $this->python_source); // save the python code to the new file
        fclose($file);

        $result = shell_exec("/usr/local/bin/python3.2.1 $filename"); // execute the code and save the result
        
        unlink($filename); // remove the file
        return $result; // return the result
    }
}

function gradeAttempt($authtoken, $examID, $submittedAnswers) {
    
    $graderSheet = [];
    // assemble the graderSheet
    // [id => weight, loop, recursion, funcname, testcases, submission} ]
    // part a: id, weight, loop, rec from exam table
    // part b: funcname, testcases from question table
    // part c: submission from $submittedAnswers
    
    // part a
    $data = array("authtoken"=>$authtoken,"examID"=>$examID);
    $examQuestionInfo = parse_backend_response(sendBackendRequest("getExamQuestions", $data));

    if ($examQuestionInfo->statusCode == 0) {
        return new Response(400, "Error: " . $examQuestionInfo->errorMessage);
    } else {
//        return new Response(200, $examQuestionInfo);
    }
    
    foreach ($examQuestionInfo->questions as $value) {
        $graderSheet[$value->questionID] = array(
            "weight" => $value->weight,
            "checkLoops" => $value->checkLoops,
            "checkRecursion" => $value->checkRecursion,
            "submission" => ""
            );
    }
    
    // part b
    foreach ($graderSheet as $key => $value) {

        // get question data - function name and test case array
        $qid = $key;
        $data = array("authtoken"=>$authtoken,"questionID"=>$qid);
        $question = parse_backend_response(sendBackendRequest("getQuestion", $data));
        
        if ($question->statusCode == 0) {
            return new Response(400, "Error: " . $question->errorMessage);
        } else {
//            return new Response(200, $question);
        }

        $graderSheet[$qid] = array_merge($graderSheet[$qid], array(
            "text" => $question->text,
            "funcname" => $question->funcname,
            "testcases" => $question->testcases
            ));
    }
    
    // part c
    foreach ($submittedAnswers as $value) {
//        $value = json_encode($value);
//        $value = str_replace("\r", "", $value);
//        $value = json_decode($value);
        $graderSheet[$value->questionID]["submission"] = $value->submission;
    }
    
//    var_dump($graderSheet);
//    exit(0);
    
    // graderSheet assembled - work through and grade each question
    $examScore = 0;
    $examMaxScore = 0;
    $examFeedback = [];
    foreach ($graderSheet as $questionID => $question) {
        $testcasesCorrect = 0;
        $testcasesMax = 0;
        $questionFeedback = "";
        $questionText = $question["text"];
        $weight = $question["weight"];
        $checkLoops = $question["checkLoops"];
        $checkRecursion = $question["checkRecursion"];

        // code to test
        $code = $question["submission"];
        $funcname = $question["funcname"];
        $testcases = $question["testcases"];

        // run the code against each test case
        foreach($testcases as $testcase) {
            $testcasesMax++;
            $output = $testcase->output;
            $input = (array) $testcase->input;

            // create a print function call from funcname and input
            $inputString = "print($funcname(";
            foreach($input as $key => $value) {
                if ($key > 0) {
                    $inputString = $inputString . ", $value";
                } else {
                    $inputString = $inputString . $value;
                }
            }
            $inputString = $inputString . "))";
            
            // concatenate to code and attempt to run
            $trial = "$code\n$inputString\n# expect $output";
//            echo "<pre>$trial</pre>";
            $pr = new PythonRunner($trial);
            if ($pr->isSafeToRun()) {
                $result = $pr->execute();
                $result = substr($result, 0, -1); // remove \n
//                echo "<pre>$result vs. $output</pre>";
                if (strcmp($result, $output) == 0) {
                    $testcasesCorrect++;
                }
            }
        }
        $questionFeedback .= "This question was worth a total of $weight points.";
        $questionScore = $testcasesCorrect / $testcasesMax * $weight;
        $questionFeedback .= "\n$testcasesCorrect of $testcasesMax test cases correct. (" . $questionScore . " points)";
        
        $deductions = 0;
        if ($checkLoops) {
            if (!strpos($code, "for") && !strpos($code, "while")) {
                $deductions++;
                $questionFeedback .= "\nThis question required loops, but we couldn't find any in your submission. (-" . ($questionScore * .25) . " points)";
            }
        }
        
        if ($checkRecursion) {
            $offset = strpos($code, $funcname);
            if (strpos($code, $funcname, $offset + 1) === false) {
                $deductions++;
                $questionFeedback .= "\nThis question required recursion, but we couldn't find any in your submission. (-" . ($questionScore * .25) . " points)";
            }
        }
        
        $questionScore -= $deductions * .25 * $questionScore;
        
        $percent = ($questionScore / $weight) * 100;
        $questionFeedback .= "\nTotal score for this question: " . $questionScore . " out of $weight ($percent%)";
        $examScore += $questionScore;
        $examMaxScore += $weight;
        $examFeedback[] = array("questionID"=> $questionID, "questionText"=>$questionText, "feedback" =>$questionFeedback);
    }
    $score = ceil(($examScore / $examMaxScore) * 100);
//    echo $score;
//    exit(0);
    $data = array("authtoken"=>$authtoken, "examID"=>$examID, "score"=>$score, "answers"=>json_encode($submittedAnswers),"feedback"=>json_encode($examFeedback));
    $result = parse_backend_response(sendBackendRequest("createAttempt", $data));
    
    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

?>
