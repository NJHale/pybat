<?php

function authenticate($username, $password) {
    $data = array('username'=>$username, 'password'=>$password);
    $result = parse_backend_response(sendBackendRequest("authenticate", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function register($username, $password, $accountType, $firstname, $lastname) {
    $data = array('username'=>$username, 
                'password'=>$password, 
                'accountType'=>$accountType, 
                'firstname'=>$firstname, 
                'lastname'=>$lastname);

    $result = parse_backend_response(sendBackendRequest("register", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getUsername($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getUsername", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getRole($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getRole", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function getName($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("getName", $data));

    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

function deauthenticate($authtoken) {
    $data = array('authtoken'=>$authtoken);
    $result = parse_backend_response(sendBackendRequest("deauthenticate", $data));
    
    if ($result->statusCode === 0) {
        return new Response(400, "Error: " . $result->errorMessage);
    } else {
        return new Response(200, $result);
    }
}

?>
