<?php

/**
 * A template for standardizing responses to the front end. 
 * Takes two arguments:
 * 1 - The HTTP response code
 * 2 - The body of the response
 */
class Response {
    public $http_status_code;
    public $response_body;

    function __construct($code, $body) {
        $this->http_status_code = $code;
        $this->response_body = $body;
    }
}

/**
 * Outputs a response object and exits
 */
function sendResponseAndExit($responseObject) {
    http_response_code($responseObject->http_status_code);
    echo json_encode($responseObject->response_body);
    echo "\n";
    exit();
}

/**
 * Standardizes the sending of requests to the backend
 * Returns the response
 */
function sendBackendRequest($action, $arrayOfRequestProperties) {
    $backend_url = 'https://web.njit.edu/~dkp35/backend.php';
    $request_content = json_encode(array_merge(array('action'=>$action), $arrayOfRequestProperties), JSON_FORCE_OBJECT);

    $req = curl_init(); 
    curl_setopt($req, CURLOPT_URL, $backend_url); 
    curl_setopt($req, CURLOPT_RETURNTRANSFER, TRUE); 
    curl_setopt($req, CURLOPT_POST, TRUE);
    curl_setopt($req, CURLOPT_POSTFIELDS, $request_content); 
    curl_setopt($req, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

    return curl_exec($req);
}

/**
 * A function for standardizing how responses from the backend are parsed.
 * Currently, the json is just decoded and returned
 */
function parse_backend_response($be_response) {
    return json_decode($be_response);
}

/**
 * A function for standardizing how requests from the front end are parsed.
 * Currently, the json is just decoded and returned
 */
function parse_frontend_request($fe_request) {
    return json_decode($fe_request);
}

?>
