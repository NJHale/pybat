//Nick Hale njh4@njit.edu

$( document ).ready(function() {
	//default fill exam-selection-table
	fillExamSelectionTable()
	$('#view-scores').hide()
	$('#take-exams').show()
	
	$('ul.nav > li').click(function (e) {
            e.preventDefault();
            $('ul.nav > li').removeClass('active');
            $(this).addClass('active');                
     });      
});

//START - handle nav links ----------------------------------------------------------------------------------------------------------------
$('#take-exams-lnk').on('click', function() {
	fillExamSelectionTable()
	$('#warning-box').hide()
	$('#success-box').hide()
	$('#view-scores').hide()
	$('#take-exams').show()
});

$('#view-scores-lnk').on('click', function() {
	fillExamScoresTable()
	$('#view-scores').show()
	$('#take-exams').hide()
	$('#warning-box').hide()
	$('#success-box').hide()
	$("#selected-exam-div").hide()
});

$("#logout-lnk").on('click', function() {
		//send a deauth request
		var data = {
			'action': 'deauthenticate',
			'authtoken': sessionStorage.authtoken
		};
		sessionStorage.authtoken = null;
		//redirect to login
		redirect('login.html');
		//TODO: add authenticate
});
//END - handle nav links -------------------------------------------------------------------------------------------------------------------


//START - take exam area -----------------------------------------------------------------------------------------------------------------
	var curExamID;
	var selectExamClick = function(obj) {
		//remove old questions
		$('#exam-selection-table').hide()
		$('#exam-form').children('.dynIn').remove()
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		var examID = $this.parent().siblings(".col-second").text()
		var examName = $this.parent().siblings(".col-first").text()
		
		$('#exam-name').text(examName)
		$('#exam-name').show()
		$('#exam-form').show()
		
		var data = {
			'authtoken': sessionStorage.authtoken,
			'action': 'getExamQuestions',
			'examID': examID,
		};
		curExamID = examID;
		sendAjaxRequest(data, getExamQuestionsCallback)
	}
	
	//declare array to hold questionIDs
	var questionIDs = []
	function getExamQuestionsCallback (data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				//clear questionIDs
				questionIDs = [];
				for (q in data.questions) {
					var question = data.questions[q]
					var qData = {
						'action': 'getQuestion',
						'authtoken': sessionStorage.authtoken,
						'questionID': question.questionID
					};
					//create vars to show whether or not to use recursion and or looping
					var useRecursion = '';
					var useLoops = '';
					//set them if they are true
					if (question.checkRecursion)
						useRecursion = '<span>You must utilize recursion in your answer.</span><br />'
					if (question.checkLoops)
						useLoops =  '<span>You must utilize looping in your answer.</span><br />'
					
					//append dynamic question div and fill with recursion and loop check data
					$('#exam-form').prepend('<div id="questionDiv-' + question.questionID + '" class="dynIn container-fluid">'
												+ useRecursion
												+ useLoops
												+ '<textarea id="textarea-' + question.questionID + '" class="tabbed-textarea col-md-12" name="submission" required>Write Python Function Here</textarea></div><hr></hr>');
					//hide the dynamic question div until the callback function is called
					$('#questionDiv-' + question.questionID).hide()
					//make AJAX call
					sendAjaxRequest(qData, getExamQuestionCallback)
				}
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getExamQuestionCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				questionIDs = [data.questionID].concat(questionIDs)
				//prepend the rest of the question data to the associated question div
				$('#questionDiv-' + data.questionID).prepend('<intput type="text" name="questionID" value="' + data.questionID + '">'
												+ '<div class="alert alert-info col-md-12">' + data.text + '</div>');
				//show the question div 
				$('#questionDiv-' + data.questionID).show()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function fillExamSelectionTable() {
		//prepare request data
		var data = {
			'action': 'getAvailExamIDs',
			'authtoken': sessionStorage.authtoken
		}
		$('#exam-selection-table tr').slice(1).remove();
		//make AJAX request
		sendAjaxRequest(data, getAvailExamIDsCallback);
		//show the exams table
		$('#exam-selection-table').show()
	}
	
	function getExamNameCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$('#exam-selection-table').append('<tr>'
					+ '<td class="col-first">' + data.examName + '</td>'
					+ '<td class="col-second">' + data.examID + '</td>'
					+ '<td><button class="btn btn-primary" onclick="selectExamClick(this)">Take</button></td>'
					+ '</tr>');
				
				$('#exam-selection-table').show()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	} 

	function getAvailExamIDsCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				for (examID in data.examIDs) {
					var eData = {
						'action': 'getExamName',
						'authtoken': sessionStorage.authtoken,
						'examID': data.examIDs[examID]
					};
					sendAjaxRequest(eData, getExamNameCallback)
				}
				
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}

	}
	
	$('#exam-form').submit(function(event) {
		//prevent default submission
		event.preventDefault()
		var submissionData = serializeObject("#exam-form").submission
		//if an exam only has one question
		if (questionIDs.length == 1)
			submissionData = [submissionData]
		var submittedAnswers = []
		//questionIDs and submissionData should be the same length
		for (var i = 0; i < questionIDs.length; i++) {
			var data = {
				"questionID": questionIDs[i], 
				"submission": $('#textarea-' + questionIDs[i]).val() 
			};
			//push into array
			submittedAnswers.push(data)
		}
		
		var data = {
			"action": "gradeAttempt",
			"authtoken": sessionStorage.authtoken,
			"examID": curExamID,
			"submittedAnswers": submittedAnswers
		};
		sendAjaxRequest(data, gradeAttemptCallback)
		//remove questions
		$('#exam-form').removeClass('.dynIn')
		return false
	});
	
	function gradeAttemptCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				$("#success-text").text("Submit Exam Successful. I never doubted you! Wonderful!")
				$("#success-box").show()
				$("#warning-box").hide()
				$("#success-box").focus()
				$('#exam-form').hide()
				$('#exam-name').hide()
				fillExamSelectionTable();
				break
			case 400://general failure status
				//TODO: add general failure status handling
				$("#warning-text").text("Submit Exam Failed. Your weapon… you will not need it.")
				$("#warning-box").show()
				$("#warning-box").focus()
				$("#success-box").hide()
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
				$("#success-box").hide()
		}
	}
//END - take exam area -----------------------------------------------------------------------------------------------------------------

//START - view scores area -------------------------------------------------------------------------------------------------------------

	function fillExamScoresTable() {
		var data = {
			'action': 'getScoresByUser',
			'authtoken': sessionStorage.authtoken
		}
		$('#exam-score-table tr').slice(1).remove();
		sendAjaxRequest(data, getUserExamScoresCallback);
	}

	function getUserExamScoresCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				if (data.examIDs !== null) {
					for (var i = 0; i < data.examIDs.length; i++) {
						$('#exam-score-table').append('<tr>'
						+ '<td class="col-first">' + data.examNames[i] + '</td>'
						+ '<td class="col-second">' + data.examIDs[i] + '</td>'
						+ '<td>' + data.scores[i] + '</td>'
						+ '<td><button class="btn btn-primary" onclick="examFeedbackClick(this)">+</button></td>'
						+ '</tr>');
					}
				}
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	var examFeedbackClick = function(obj) {
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		//get id and name
		var examID = $this.parent().siblings(".col-second").text()
		currentExamID = examID;
		var examName = $this.parent().siblings(".col-first").text()
		$("#selected-exam-div").hide()
		$("#selected-exam-name").text(examName)
		//make ajax call
		var data = {
			'action': 'getFeedback',
			'authtoken': sessionStorage.authtoken,
			'examID': examID
		}
		sendAjaxRequest(data, getFeedbackCallback)
	}
	
	function getFeedbackCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				$('#selected-exam-feedback').children().remove()
				for (var i = 0; i < data.feedback.length; i ++) {
					$('#selected-exam-feedback').append('<div class="alert alert-info"><p>' + data.feedback[i].questionText + '</p><br />'
					 + '<p>' + data.feedback[i].feedback + '</p></div><hr>')
				}
				$("#selected-exam-div").show()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
//END - view scores area
