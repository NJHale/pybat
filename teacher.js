//Nick Hale njh4@njit.edu
$( document ).ready(function() {
	//if the authtoken is null redirect to login
	if (sessionStorage.authtoken == null) {
		redirect("login.html")
	}
	//otherwise fill the question bank
	fillQuestionBank()
	
	$('ul.nav > li').click(function (e) {
            e.preventDefault();
            $('ul.nav > li').removeClass('active');
            $(this).addClass('active');                
     });      
});
//add delete-btn class on click handler
//define outside for global accessability
    var deleteQuestionFunc = function(obj) {
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		var questionID = $this.parent().siblings(".first-col").text()

		var data = {
			'authtoken': sessionStorage.authtoken,
			'action': 'deleteQuestion',
			'questionID': questionID
		};
		sendAjaxRequest(data, deleteQuestionCallback)
	}
	
    //$( document ).ready(function(){
	//fill the question bank by default
	//create a JSON object to hold question data
	var questionData = {};

	//start - handle nav link clicks
	$("#manage-questions-lnk").on('click', function() {
		$('#manage-questions').show();
		$('#manage-exams').hide();
		$('#take-exams').hide();
		$('#exam-stats').hide();
		$("#success-box").hide()
		$("#warning-box").hide()
		fillQuestionBank()
		//TODO: add function call to populate stuff
	});
	
	$("#manage-exams-lnk").on('click', function() {
		$('#manage-questions').hide();
		$('#manage-exams').show();
		$('#take-exams').hide();
		$('#exam-stats').hide();
		$("#success-box").hide()
		$("#warning-box").hide()
		//fill the exam bank
		fillExamBank()
	});
	/*
	$("#take-exams-lnk").on('click', function() {
		$('#manage-questions').hide();
		$('#manage-exams').hide();
		$('#take-exams').show();
		$('#exam-stats').hide();
		$("#success-box").hide()
		$("#warning-box").hide()
		//TODO: add function call to populate stuff
	});*/
	
	$("#exam-stats-lnk").on('click', function() {
		$('#manage-questions').hide();
		$('#manage-exams').hide();
		$('#take-exams').hide();
		$('#exam-stats').show();
		$("#success-box").hide()
		$("#warning-box").hide()
		//fill the exam stats table
		fillExamStatsTable();
	});
	
	$("#logout-lnk").on('click', function() {
		//send a deauth request
		var data = {
			'action': 'deauthenticate',
			'authtoken': sessionStorage.authtoken
		};
		sessionStorage.authtoken = null;
		//redirect to login
		redirect('login.html');
		//TODO: add authenticate
	})
	//end - handle nav link clicks
	
	//add question button handler
	$("#create-question-btn").on('click', function() {
		questionData.action = "createQuestion"
		questionData.authtoken = sessionStorage.authtoken
		
		//alert(JSON.stringify(questionData))
		//hide status boxes
		$("#success-box").hide()
		$("#warning-box").hide()
		$("#testcase-form").hide()
		$("#current-testcases").hide()
		$('#create-question-btn').hide()
		//TODO: add try-catch
		//make AJAX call
		sendAjaxRequest(questionData, createQuestionCallback)
	});
	
	//start - question form functions
	$("#header-form").submit(function(event){
		//alert("here")
		//prevent default submission
		event.preventDefault()
		//hide all status boxes and testcases
		$("#success-box").hide()
		$("#warning-box").hide()
		$("#testcase-form").hide()
		$("#current-testcases").hide()
		$('#create-question-btn').hide();
		//drop old testcases
		$('#current-testcases tr').slice(1).remove();
		//clear question data
		questionData = {
			'text': null,
			'funcname': null,
			'parameters': [],
			'testcases': []
		};
		//gather form data
		var formData = serializeObject('#header-form');
		//place description in questionData
		questionData.text = formData.description
		//create function header regex
		var regex = /([A-Za-z]+[0-9]*)(\((([A-Za-z]+[0-9]*)+(,[A-Za-z]+[0-9]*)*)*\))/g;
		//test to see if the header is valid
		var validHeader = regex.test(formData.header.replace(/ /g,''))
		if (validHeader) {
			//add function name to the request data
			questionData.funcname = formData.header.substr(0, formData.header.indexOf('(')); 
			//pull all params out of the header if any
			var params = tokenizeHeader(formData.header);
			//add testcases to question forms
			//show the add-question-btn
			addTestCasesToQuestionForm(params)
		} else {
			$("#warning-text").text("Invalid function definition! That’s no moon...")
			$("#warning-box").show()
			$("#warning-box").focus()
		}
		
		return false;
	});
	
	$("#testcase-form").submit(function(event){
		//prevent default submission
		event.preventDefault()
		$('#create-question-btn').show();
		var formData = serializeObject('#testcase-form')
		formData.input = [formData.input].reverse()
		//push the forms JSON object into the request JSON 
		questionData.testcases.push(formData)
		//now append that data to the json to the testcases list
		$('#current-testcases').append('<tr><td>' + formData.input + ' </td>'
														+'<td>' + formData.output + ' </td></tr>');
		$('#current-testcases').show();
		return false; 
	});
	
	
	//end - question form functions
	
	//add a testcase to the question form
	function addTestCasesToQuestionForm(params) {
		//remove any input fields persisting from the last question
		$('input', '#testcase-form').remove();
		$('#testcase-form').prepend('<input class="col-md-12" type="text" name="output" value="" placeholder="output" required></input>');
		if (params !== null) {
			for (param in params) {//add parameter input fields
				//add parameter to JSON request data
				questionData.parameters.push(params[param])
				$('#testcase-form').prepend('<input class="col-md-12" type="text" value="" name="input" placeholder="value of ' 
					+ params[param] + '" required></input>');
			}
		}
		else {//else push a null value into the return data
			questionData.parameters.push(null)
		}
		$('#testcase-form').show();
	}
	
	
	function fillQuestionBank() {
		//first get all questionIDs that belong to the user
		var data = {
			'action': 'getQuestionIDs',
			'authtoken': sessionStorage.authtoken
		}
		$('#question-bank-table tr').slice(1).remove();
		sendAjaxRequest(data, getQuestionIDsCallback)
	}
	
	function deleteQuestionCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				//set success text
				$("#success-text").text("Delete Question Success! …I think I just blasted it.")
				$("#success-box").show()
				$("#success-box").focus()
				//refill the question bank
				fillQuestionBank()
				break
			case 400://general failure status
				$("#warning-text").text("Delete Failed. It’s not impossible. I used to bullseye womp rats in my T-16 back home, they’re not much bigger than two meters.")
				$("#warning-box").show()
				$("#warning-box").focus()
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getQuestionCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				//append data to table
				//alert(data.questionID)
				$('#question-bank-table').append('<tr>'
					+ '<td class="first-col">' + data.questionID + '</td>'
					+ '<td>' + data.text + '</td>'
					+ '<td>' + data.funcname + '</td>'
					+ '<td>' + JSON.stringify(data.parameters) + '</td>'
					+ '<td><button class="btn btn-primary delete-question-btn" onclick="deleteQuestionFunc(this)">delete</button></td>'
					+ '</tr>');
				break
			case 400://general failure status
				console.log(data.error)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getQuestionIDsCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				//append data to table
				for (id in data.questionIDs) {
					var qData = {
						'action': 'getQuestion',
						'authtoken': sessionStorage.authtoken,
						'questionID': data.questionIDs[id]
					};
					sendAjaxRequest(qData, getQuestionCallback)
				}
				break
			case 400://general failure status
				console.log(data.error)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function createQuestionCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				//append data to table
				$("#success-text").text("Add Question Success! Only at the end do you realize the power of the Dark Side.")
				$("#success-box").show()
				$("#success-box").focus()
				var qData = {
					'action': 'getQuestion',
					'authtoken': sessionStorage.authtoken,
					'questionID': data.questionID
				};
				sendAjaxRequest(qData, getQuestionCallback)
				break
			case 400://general failure status
				$("#warning-text").text("Add Question Failed.…You’ve failed, your highness. I am a Jedi, as my father was before me.")
				$("#warning-box").show()
				$("#warning-box").focus()
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	//end - Manage Question Area -----------------------------------------------------------------------------------------
	
	//start - Manage Exam Area -------------------------------------------------------------------------------------------
	
	
	
	$('#create-exam-form').submit(function(event) {
		//prevent default submission
		event.preventDefault();
		//hide status boxes etc...
		$("#success-box").hide()
		$("#warning-box").hide()
		$("#exam-bank-table").hide()
		
		//populate the JSON request data
		var data = serializeObject('#create-exam-form')
		data.authtoken = sessionStorage.authtoken
		data.action = "createExam"
		//send the AJAX call
		sendAjaxRequest(data, createExamCallback)
		return false;
	});
	
	function createExamCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				var eData = {
					'action': 'getExamName',
					'authtoken': sessionStorage.authtoken,
					'examID': data.examID
				};
				sendAjaxRequest(eData, getExamNameCallback)
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getExamIDsCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				for (eid in data.eids) {
					var eData = {
						'action': 'getExamName',
						'authtoken': sessionStorage.authtoken,
						'examID': data.eids[eid]
					};
					sendAjaxRequest(eData, getExamNameCallback)
				}
				
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}

	}
	
	
	
	//holds the current exam being managed
	var currentExamID;
	var manageExamClick = function(obj) {
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		//get id and name
		var examID = $this.parent().siblings(".col-second").text()
		currentExamID = examID;
		var examName = $this.parent().siblings(".col-first").text()
		$("#selected-exam-name").text(examName)
		$("#exam-open-box").hide()
		$("#exam-closed-box").hide()
		$("#selected-exam-box").show()
		//populate the tables
		populateExamManagementTables()
		//make ajax call
		var data = {
			'action': 'getExamStatus',
			'authtoken': sessionStorage.authtoken,
			'examID': examID
		}
		sendAjaxRequest(data, getExamStatusCallback)
	}
	
	function getExamStatusCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				if (data.status == "hidden" || data.status == "closed") {
					$("#exam-closed-box").show()
					$("#exam-open-box").hide()
				}
				else {
					$("#exam-closed-box").hide()
					$("#exam-open-box").show()
				}
				
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	$("#exam-open-btn").on('click', function() {
		var data = {
			'action': 'openExam',
			'authtoken': sessionStorage.authtoken,
			'examID': currentExamID
		};
		sendAjaxRequest(data, openExamCallback)
		
	});
	
	$("#exam-close-btn").on('click', function() {
		var data = {
			'action': 'closeExam',
			'authtoken': sessionStorage.authtoken,
			'examID': currentExamID
		};
		sendAjaxRequest(data, closeExamCallback)
		
	});
	
	function closeExamCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$("#exam-closed-box").show()
				$("#exam-open-box").hide()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function openExamCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$("#exam-closed-box").hide()
				$("#exam-open-box").show()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getExamNameCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$('#exam-bank-table').append('<tr>'
					+ '<td class="col-first">' + data.examName + '</td>'
					+ '<td class="col-second">' + data.examID + '</td>'
					+ '<td><button onclick="manageExamClick(this)" class="btn btn-primary inspect-exam-btn">+</button></td>'
					+ '</tr>');
				//show the exam bank table
				$('#exam-bank-table').show();
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	} 
	
	function fillExamBank() {
		var data = {
			'action': 'getExamIDsByTeacher',
			'authtoken': sessionStorage.authtoken
		}
		$('#exam-bank-table tr').slice(1).remove();
		sendAjaxRequest(data, getExamIDsCallback);
	}
	
	function populateExamManagementTables() {
		$('#question-on-exam-table tr').slice(1).remove();
		$('#question-off-exam-table tr').slice(1).remove();
		var data = {
			'action': 'getUnusedQuestions',
			'authtoken': sessionStorage.authtoken,
			'examID': currentExamID
		}
		sendAjaxRequest(data, getUnusedQuestionsCallback);
		data = {
			'action': 'getExamQuestions',
			'authtoken': sessionStorage.authtoken,
			'examID': currentExamID
		}
		sendAjaxRequest(data, getExamQuestionsCallback);
		//alert(currentExamID)
	}
	
	function getExamQuestionsCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				for (q in data.questions) {
					//pull the question JSON object out of the array
					var question = data.questions[q]
					//instantiate the request payload
					var qData = {
						'action': 'getQuestion',
						'authtoken': sessionStorage.authtoken,
						'questionID': question.questionID
					};
					//append a new question row to the on exam table ID'd with the questionID
					$('#question-on-exam-table').append('<tr id="questionRow-' + question.questionID + '">'
					+ '<td>' + question.weight + '</td>'
					+ '<td>' + question.checkRecursion + '</td>'
					+ '<td>' + question.checkLoops + '</td>'
					+ '<td><button class="btn btn-primary" onclick="removeQuestionFromExamClick(this)">-</button></td>'
					+ '</tr>');
					//hide the row until the rest of the question is retrieved
					$('questionRow-' + question.questionID).hide();
					sendAjaxRequest(qData, getExamQuestionCallback)
				}
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function addQuestionToExamCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				//re-populate tables
				populateExamManagementTables()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	var addQuestionToExamClick = function(obj) {
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		var questionID = $this.parent().siblings(".first-col").text()

		var data = {
			'authtoken': sessionStorage.authtoken,
			'action': 'addQuestion',
			'examID': currentExamID,
			'questionID': questionID,
			'weight': $('#weight-' + questionID).val(),
			'checkRecursion': $('#checkRecursion-' + questionID).is(':checked'),
			'checkLoops': $('#checkLoops-' + questionID).is(':checked')
		};
		sendAjaxRequest(data, addQuestionToExamCallback)
	}
	
	var removeQuestionFromExamClick = function(obj) {
		var element = obj;  // This is the DOM object being clicked
		var $this = $(obj); // This is the jQuery object being clicked
		var questionID = $this.parent().siblings(".first-col").text()

		var data = {
			'authtoken': sessionStorage.authtoken,
			'action': 'removeQuestion',
			'examID': currentExamID,
			'questionID': questionID
		};
		sendAjaxRequest(data, removeQuestionFromExamCallback)
	}
	
	function removeQuestionFromExamCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				//re-populate tables
				populateExamManagementTables()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getExamQuestionCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
			    //prepend the remaining data to the associated question row
				$('#questionRow-' + data.questionID).prepend('<td class="first-col">' + data.questionID + '</td>'
					+ '<td>' + data.text + '</td>'
					+ '<td>' + data.funcname + '</td>'
					+ '<td>' + JSON.stringify(data.parameters) + '</td>');
				//show the question row 
				$('questionRow-' + data.questionID).show();
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getUnusedQuestionCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$('#question-off-exam-table').append('<tr>'
					+ '<td class="first-col">' + data.questionID + '</td>'
					+ '<td>' + data.text + '</td>'
					+ '<td>' + data.funcname + '</td>'
					+ '<td>' + JSON.stringify(data.parameters) + '</td>'
					+ '<td><form oninput="weightVal.value=weight.value"><input type="range" name="weight" id="weight-' + data.questionID + '" value="5 min="1" max="10">'
					+ '<output name="weightVal" for="weight-' + data.questionID + '">5</output></form></td>'
					+ '<td><input type="checkbox" id="checkRecursion-' +data.questionID + '"></td>'
					+ '<td><input type="checkbox" id="checkLoops-' +data.questionID + '"></td>'
					+ '<td><button class="btn btn-primary" onclick="addQuestionToExamClick(this)">+</button></td>'
					+ '</tr>');
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getUnusedQuestionsCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				for (id in data.questionIDs) {
					var qData = {
						'action': 'getQuestion',
						'authtoken': sessionStorage.authtoken,
						'questionID': data.questionIDs[id]
					};
					//alert(data.questionIDs[id])
					sendAjaxRequest(qData, getUnusedQuestionCallback)
				}
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	//end - Manage Exam Area --------------------------------------------------------------------------------------------
	
	//start - Exam Stats Area ---------------------------------------------------------------------------------------------
	function fillExamStatsTable() {
		$('#exam-stats-table tr').slice(1).remove();
		var data = {
			'action': 'getExamIDsByTeacher',
			'authtoken': sessionStorage.authtoken
		}
		$('#exam-stats-table tr').slice(1).remove();
		sendAjaxRequest(data, getExamIDsForStatsCallback);
	}
	
	function getExamIDsForStatsCallback(data, status) {
		//state machine for AJAX call status
		switch (status) {
			case 200://successful status
				for (eid in data.eids) {
					var eData = {
						'action': 'getExamName',
						'authtoken': sessionStorage.authtoken,
						'examID': data.eids[eid]
					};
					sendAjaxRequest(eData, getExamNameForStatsCallback)
				}
				
				break
			case 400://general failure status
				//TODO: add general failure status handling
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	
	function getExamNameForStatsCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$('#exam-stats-table').append('<tr id="statsRow-' + data.examID + '">'
					+ '<td>' + data.examID + '</td>'
					+ '<td>' + data.examName + '</td>'
					+ '</tr>');
				$('#statsRow-' + data.examID).hide()
				var eData = {
					'action': 'getExamStats',
					'authtoken': sessionStorage.authtoken,
					'examID': data.examID
				}
				sendAjaxRequest(eData, getExamStatsCallback)
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	} 
	
	function getExamStatsCallback(data, status) {
		//state machine for AJAX call status 
		switch (status) {
			case 200://successful status
				$('#statsRow-' + data.examID).append('<td>' + data.n + '</td>')
				$('#statsRow-' + data.examID).append('<td>' + data.min + '</td>')
				$('#statsRow-' + data.examID).append('<td>' + data.max + '</td>')
				$('#statsRow-' + data.examID).append('<td>' + data.mean + '</td>')
				$('#statsRow-' + data.examID).append('<td>' + data.sd + '</td>')
				//show the exam stats table
				$('#exam-stats-table').show();
				$('#statsRow-' + data.examID).show()
				break
			case 400://general failure status
				//TODO: add general failure status handling
				console.log(status)
				break
			default:
				$("#warning-text").text("There are some code issues. Ungh. And I thought they smelled bad on the outside.")
				$("#warning-box").show()
				$("#warning-box").focus()
		}
	}
	//end - Exam Stats Area ----------------------------------------------------------------------------------------------
//});
