//utility javascript functions for cs490 project

	
	function redirect(dest) {
			window.location = dest;
	}
	
	//python function header tokenizer
	function tokenizeHeader(header) {
		//pull all params out of the header if any
			var regex = /\(([^)]+)\)/
			var inside = regex.exec(header.replace(/ /g,''))
			var rtn = []//instantiate return array
			if (inside !== null && inside.length > 1) {//this means that we have parameters
				//index 1 represents the values inside of the parentheses
				var innerVals = inside[1]
				//now split to see if we have multiple parameters
				var params = innerVals.split(',');
				//length greater than 1 that means we have multiple values. 
				if (params.length > 0) {
					return params
				} else {
					return rtn.push(innerVals)
				}
			}
			else {//we have no parameters return null
				return null
			}
	}
	
	function serializeObject(selector) {
		var obj = $(selector);
		var o = {};
		var a = obj.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
	
	//function that uses AJAX to send an http request for forwarding to front.php
	function sendAjaxRequest(data, callback) {
			//check to make sure the callback is a function
			//alert(JSON.stringify(data)) 
			if (!($.isFunction(callback))) {
				throw "callback is not a valid javascript function. Underestimated my power you have."
			} else {
				$.ajax({//create an ajax HTTP POST request
					type: 'POST',
					dataType: "json",
					contentType: 'application/json',
					url: 'front.php',
					data: JSON.stringify(data),
					success: function(data){
						callback(data, 200)//200 - successful
					},
					error: function(xhr, desc, err) {
						callback(data, xhr.status)//everything else - failure
					}
				}); // end ajax call
			}		
	}
	
	//allows the use of tab in a text area of dynamicly created elements of the 
	//class .tabbed-textarea
	$('body').on('keydown', '.tabbed-textarea', function(e) { 
		//get code of the key pressed
		var keyCode = e.keyCode || e.which; 
	    //if a tab is pressed
		if (keyCode == 9) { 
			// get cursor position
			var start = this.selectionStart;
			var end = this.selectionEnd;

			var $this = $(this);
			var value = $this.val();

			// concat the tab and suture together previously existing string
			$this.val(value.substring(0, start)
						+ "\t"
						+ value.substring(end));

			// put caret at right position again (add one for the tab)
			this.selectionStart = this.selectionEnd = start + 1;

			// prevent the default action from being carried out
			e.preventDefault();
		  } 
	});


